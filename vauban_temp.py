#!/usr/local/bin/python3.11

import requests
import json
import datetime
from lxml import html

now = datetime.datetime.now()

print("in St.Georgen auf stereos Balkon:")
try:
    print(" " +  html.parse('http://192.168.178.100').xpath('//body')[0].text_content() )
    response = json.loads(requests.get("https://api.open-meteo.com/v1/forecast?latitude=47.98&longitude=7.81&hourly=temperature_2m,rain").text)
    print("5h forecast:")
    print(response['hourly']['temperature_2m'][(now.hour+1):(now.hour+5)])
    count = 0
    for entry in response['hourly']['rain'][now.hour:(now.hour+5)]:
        if not entry == 0.0:
            print("#rain")
            print(entry)
except:
    response = json.loads(requests.get("https://api.opensensemap.org/boxes/5a48c50edf5df2001a573f5d?format=json").text)
    print(response["sensors"][2]["lastMeasurement"]["value"])

