# Bibliotheken laden
from machine import Pin
from utime import sleep
from dht import DHT22
import network
import socket

ssid = 'MYSSID' #Your network name
password = '*******************' #Your WiFi password

# Initialisierung GPIO und DHT22
sleep(1)
dht22_sensor = DHT22(Pin(14, Pin.IN, Pin.PULL_UP))


def connect():
    #Connect to WLAN
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ssid, password)
    while wlan.isconnected() == False:
        print('Waiting for connection...')
        sleep(1)
    ip = wlan.ifconfig()[0]
    print(f'Connected on {ip}')
    return ip

def open_socket(ip):
    # Open a socket
    address = (ip, 80)
    connection = socket.socket()
    connection.bind(address)
    connection.listen(1)
    return connection

def webpage(reading):
    #Template HTML
    html = f"""
            <!DOCTYPE html>
            <html>
            <head>
            <title>Pico W Weather Station</title>
            <meta http-equiv="refresh" content="10">
            </head>
            <body>
            <p>{reading}</p>
            </body>
            </html>
            """
    return str(html)

def serve(connection):
    #Start a web server
    
    while True:
        dht22_sensor.measure()
        # Werte lesen
        temp = dht22_sensor.temperature()
        humi = dht22_sensor.humidity()
        reading = 'Temperature: ' + str(temp) + '. Humidity: ' + str(humi) + '. '
        client = connection.accept()[0]
        request = client.recv(1024)
        request = str(request)       
        html = webpage(reading)
        client.send(html)
        client.close()

try:
    ip = connect()
    connection = open_socket(ip)
    serve(connection)
except KeyboardInterrupt:
    machine.reset()




